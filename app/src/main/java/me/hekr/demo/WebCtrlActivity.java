package me.hekr.demo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;
import android.widget.RelativeLayout;

import me.hekr.hekrsdk.bean.DeviceBean;
import me.hekr.hekrsdk.bean.Global;
import me.hekr.hekrsdk.util.HekrJSSDK;

public class WebCtrlActivity extends AppCompatActivity {

    private static final String TAG = "WebCtrlActivity";

    private RelativeLayout layout;
    private WebView webView;

    private HekrJSSDK hekrJSSDK;
    private DeviceBean deviceBean;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_ctrl);

        initView();
        initData();
    }

    protected void initView() {
        layout = (RelativeLayout) findViewById(R.id.activity_web_ctrl);
        webView = (WebView) findViewById(R.id.webView);
    }

    protected void initData() {
        Intent intent = this.getIntent();
        deviceBean = (DeviceBean) intent.getSerializableExtra("deviceBean");
        loadWebPage();
    }

    /**
     * this 当前activity
     * layout 当前activity根布局
     * webView 当前布局中的webView
     * deviceBean hekr云端获取的设备信息对象
     * "" 推送消息 默认为空
     * false
     * 开始加载控制页面
     */
    private void loadWebPage() {
        hekrJSSDK = HekrJSSDK.getInstance(this, layout, webView, deviceBean, "", true);
        Global.isUDPOpen=true;
        if (hekrJSSDK != null) {
            hekrJSSDK.setWebViewPageStatusListener(new HekrJSSDK.WebViewPageStatusListener() {
                /**
                 * 可以在此添加转圈圈
                 * @param url 加载页面初始URL
                 */
                @Override
                public void onPageStarted(String url) {

                }

                /**
                 * 可在此取消转圈圈
                 * @param url 页面加载完毕URL
                 */
                @Override
                public void onPageFinished(String url) {
                }

                /**
                 * 控制页面关闭
                 */
                @Override
                public void onAllPageClose() {
                    finish();
                }


                /**
                 * @param requestCode 二维码扫描请求码
                 */
                @Override
                public void openScan(int requestCode) {

                }
            });
            hekrJSSDK.initUrl();
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (hekrJSSDK != null) {
            hekrJSSDK.onActivityResult(requestCode, resultCode, data);
        }
    }

    /**
     * 每次一次回退
     */
    @Override
    public void onBackPressed() {
        if (hekrJSSDK != null) {
            hekrJSSDK.onBackPressed();
        } else {
            finish();
        }
    }

    /**
     * 销毁控制界面
     */
    @Override
    protected void onDestroy() {
        if (hekrJSSDK != null) {
            hekrJSSDK.onDestroy();
        }
        super.onDestroy();
    }
}
