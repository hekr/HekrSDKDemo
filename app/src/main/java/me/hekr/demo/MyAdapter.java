package me.hekr.demo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import java.util.List;

/**
 * Created by hekr_xm on 2016/7/1.
 **/
public class MyAdapter extends BaseAdapter {

    private List<MsgBean> list;
    private Context context;
    private LayoutInflater layoutInflater;
    public static final int LEFT = 0;
    public static final int RIGHT = 1;


    public MyAdapter() {
    }

    public MyAdapter(List<MsgBean> list, Context context) {
        this.list = list;
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        MsgBean msgBean = list.get(position);
        if (msgBean.getType() == LEFT) {
            return LEFT;
        } else {
            return RIGHT;
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MsgBean msgBean = list.get(position);
        if (getItemViewType(position) == LEFT) {
            LViewHolder lViewHolder;
            if (convertView == null) {
                lViewHolder = new LViewHolder();
                convertView = layoutInflater.inflate(R.layout.left_command_item, parent, false);
                lViewHolder.send_msg = (TextView) convertView.findViewById(R.id.send_msg);
                convertView.setTag(lViewHolder);
            } else {
                lViewHolder = (LViewHolder) convertView.getTag();
            }
            lViewHolder.send_msg.setText(msgBean.getMsg());
        } else {
            RViewHolder rViewHolder;
            if (convertView == null) {
                rViewHolder = new RViewHolder();
                convertView = layoutInflater.inflate(R.layout.right_command_item, parent, false);
                rViewHolder.receive_msg = (TextView) convertView.findViewById(R.id.receive_msg);
                convertView.setTag(rViewHolder);
            } else {
                rViewHolder = (RViewHolder) convertView.getTag();
            }
            rViewHolder.receive_msg.setText(msgBean.getMsg());
        }

        return convertView;
    }

    public final class LViewHolder {
        public TextView send_msg;

    }

    public final class RViewHolder {
        public TextView receive_msg;

    }

}
