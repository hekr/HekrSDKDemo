package me.hekr.demo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.litesuits.common.assist.Toastor;

import me.hekr.hekrsdk.action.HekrUser;
import me.hekr.hekrsdk.action.HekrUserAction;
import me.hekr.hekrsdk.util.ConstantsUtil;
import me.hekr.hekrsdk.util.HekrCodeUtil;
import me.hekr.hekrsdk.util.SpCache;

public class RegisterByPhoneActivity extends AppCompatActivity implements View.OnClickListener, VerfyDialog.VerifyCodeSuccess {
    private EditText et_phone, et_code, et_pwd;
    private Button btn_get_code, btn_register;
    private HekrUserAction hekrUserAction;
    private String phone, pwd;
    private Toastor toastor;
    private TextView tv_pid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        initView();
        initData();
    }

    private void initData() {
        hekrUserAction = HekrUserAction.getInstance(this);
        toastor = new Toastor(this);
        btn_get_code.setOnClickListener(this);
        btn_register.setOnClickListener(this);
        tv_pid.setText(TextUtils.concat("Hekr Pid:", SpCache.getString(ConstantsUtil.HEKR_PID, "xxx")));
    }

    private void initView() {
        tv_pid = (TextView) findViewById(R.id.tv_pid);
        et_phone = (EditText) findViewById(R.id.et_phone);
        et_code = (EditText) findViewById(R.id.et_code);
        et_pwd = (EditText) findViewById(R.id.et_pwd);
        btn_get_code = (Button) findViewById(R.id.btn_get_code);
        btn_register = (Button) findViewById(R.id.btn_register);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_get_code:
                phone = et_phone.getText().toString().trim();
                if (!TextUtils.isEmpty(phone) && VerfyDialog.isMobile(phone)) {
                    VerfyDialog dialog = new VerfyDialog();
                    dialog.showDialog(this, hekrUserAction, et_phone.getText().toString().trim(), HekrUserAction.CODE_TYPE_REGISTER);
                    dialog.show();
                    dialog.setVerifyCodeSuccess(this);
                } else {
                    toastor.showSingletonToast("请输入正确的手机号");
                }
                break;

            case R.id.btn_register:
                String code = et_code.getText().toString().trim();
                pwd = et_pwd.getText().toString().trim();
                if (!TextUtils.isEmpty(code) && !TextUtils.isEmpty(pwd)) {
                    hekrUserAction.checkVerifyCode(phone, code, new HekrUser.CheckVerifyCodeListener() {
                        @Override
                        public void checkVerifyCodeSuccess(String phoneNumber, String verifyCode, String token, String expireTime) {
                            register(phoneNumber, pwd, token);
                        }

                        @Override
                        public void checkVerifyCodeFail(int errorCode) {
                            showError(errorCode);
                        }
                    });
                } else {
                    toastor.showSingleLongToast("请输入完整！");
                }
                break;
        }
    }


    private void register(String phoneNumber, String pwd, String token) {
        hekrUserAction.registerByPhone(phoneNumber, pwd, token, new HekrUser.RegisterListener() {
            @Override
            public void registerSuccess(String uid) {
                toastor.showSingleLongToast("注册成功！");
                finish();
            }

            @Override
            public void registerFail(int errorCode) {
                showError(errorCode);
            }
        });
    }

    private void showError(int errorCode) {
        toastor.showSingleLongToast(HekrCodeUtil.errorCode2Msg(errorCode));
    }

    @Override
    public void getToken(String tokenId) {
        toastor.showSingleLongToast("获取验证码成功！");
    }
}
