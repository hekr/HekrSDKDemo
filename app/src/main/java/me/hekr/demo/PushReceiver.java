package me.hekr.demo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;

import com.igexin.sdk.PushConsts;
import com.igexin.sdk.PushManager;
import com.igexin.sdk.Tag;

import me.hekr.hekrsdk.action.HekrUser;
import me.hekr.hekrsdk.action.HekrUserAction;
import me.hekr.hekrsdk.util.HekrSDK;

public class PushReceiver extends BroadcastReceiver {

    private static final String TAG = "PushReceiver";
    private HekrUserAction hekrUserAction;

    /**
     * 应用未启动, 个推 service已经被唤醒,保存在该时间段内离线消息(此时 GetuiSdkDemoActivity.tLogView == null)
     */

    @Override
    public void onReceive(Context context, Intent intent) {

        Bundle bundle = null;
        if (intent != null) {
            hekrUserAction = HekrUserAction.getInstance(context);
            bundle = intent.getExtras();
        }
        if (bundle != null) {
            switch (bundle.getInt(PushConsts.CMD_ACTION)) {
                case PushConsts.GET_MSG_DATA:

                    byte[] payload = bundle.getByteArray("payload");

                    String taskId = bundle.getString("taskid");
                    String messageId = bundle.getString("messageid");

                    PushManager.getInstance().sendFeedbackMessage(context, taskId, messageId, 90001);

                    if (payload != null) {
                        String data = new String(payload);
                        if (!TextUtils.isEmpty(data)) {
                            //接收推送数据
                            me.hekr.hekrsdk.util.Log.i(TAG, "推送数据:" + data);
                        }
                    }
                    break;

                case PushConsts.GET_CLIENTID:
                    // 获取ClientID(CID)
                    String cid = bundle.getString("clientid");

                    /*个推设置tag
                    为当前用户设置一组标签，后续推送可以指定标签名进行定向推送
                    标签的设定，一定要在获取到 Clientid 之后才可以设定。标签的设定，服务端限制一天只能成功设置一次*/
                    if (!TextUtils.isEmpty(HekrSDK.pid)) {
                        String[] tags = new String[]{HekrSDK.pid};
                        Tag[] tagParam = new Tag[tags.length];

                        for (int i = 0; i < tags.length; i++) {
                            Tag t = new Tag();
                            t.setName(tags[i]);
                            tagParam[i] = t;
                        }
                        PushManager.getInstance().setTag(context, tagParam, "100861");
                    }

                    /*接口 PushManager 中的 bindAlias， 绑定别名
                    同一个别名最多绑定10个 ClientID (适用于允许多设备同时登陆的应用)，当已绑定10个 ClientID 时，再次调用此接口会自动解绑最早绑定的记录，
                    当ClientID已绑定了别名A，若调用此接口绑定别名B，则与别名A的绑定关系会自动解除。
                    此接口与 unBindAlias 一天内最多调用100次，两次调用的间隔需大于5s*/
                    if (!TextUtils.isEmpty(hekrUserAction.getUserId())) {
                        PushManager.getInstance().bindAlias(context, hekrUserAction.getUserId());
                    }

                    //hekr服务器关联个推服务器
                    if (!TextUtils.isEmpty(cid)) {
                        hekrUserAction.pushTagBind(cid, new HekrUser.PushTagBindListener() {
                            @Override
                            public void pushTagBindSuccess() {

                            }

                            @Override
                            public void pushTagBindFail(int errorCode) {

                            }
                        });
                    }

                    break;

                case PushConsts.THIRDPART_FEEDBACK:
                    String s = bundle.getString("result");
                    long timestamp = bundle.getLong("timestamp");
                    Log.d(TAG, "result = " + s);
                    Log.d(TAG, "timestamp = " + timestamp);
                    break;

                default:
                    break;
            }
        }
    }
}