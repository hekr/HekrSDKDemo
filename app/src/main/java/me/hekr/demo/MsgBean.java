package me.hekr.demo;

/**
 * Created by hekr_xm on 2016/7/1.
 **/
public class MsgBean {
    private int type;
    private String msg;

    public MsgBean(int type, String msg) {
        this.type = type;
        this.msg = msg;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public String toString() {
        return "MsgBean{" +
                "type=" + type +
                ", msg='" + msg + '\'' +
                '}';
    }
}
