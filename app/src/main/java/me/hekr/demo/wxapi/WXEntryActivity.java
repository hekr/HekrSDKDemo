/**
 * 这里需要修改为自己的相应的包名
 */
package me.hekr.demo.wxapi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.tencent.mm.sdk.modelbase.BaseReq;
import com.tencent.mm.sdk.modelbase.BaseResp;
import com.tencent.mm.sdk.modelmsg.SendAuth;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.sdk.openapi.WXAPIFactory;

import org.greenrobot.eventbus.EventBus;

import me.hekr.hekrsdk.event.AuthCodeEvent;
import me.hekr.hekrsdk.util.HekrSDK;


/**
 * update by hekr_xm on 2016/5/3.
 **/
public class WXEntryActivity extends Activity implements IWXAPIEventHandler {

    private IWXAPI api;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        api = WXAPIFactory.createWXAPI(this, HekrSDK.getWeiXinBean().getAppId(), false);
        handleIntent(getIntent());
    }

    private void handleIntent(Intent paramIntent) {
        api.handleIntent(paramIntent, this);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        handleIntent(intent);
    }

    @Override
    public void onReq(BaseReq baseReq) {
        finish();
    }

    @Override
    public void onResp(BaseResp baseResp) {
        String code = null;
        if (baseResp.errCode == BaseResp.ErrCode.ERR_OK) {
            try {
                code = ((SendAuth.Resp) baseResp).code;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        EventBus.getDefault().post(new AuthCodeEvent(AuthCodeEvent.TYPE_WEIXIN, code));
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

}
