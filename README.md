# HekrSDKDemo
HEKR SDK 使用DEMO

> * Hekr SDK [使用指南](http://docs.hekr.me/v4/%E5%BC%80%E5%8F%91%E6%96%87%E6%A1%A3/APP%20%E5%BC%80%E5%8F%91/Android%20%E5%BC%80%E5%8F%91%E6%96%87%E6%A1%A3/)
> * Hekr SDK [docs](http://jin123d.coding.me/HekrSDKDemo)
> * Hekr SDK DEMO APK[点此下载](app-pid-debug.apk)【此APK无推送，可手动修改PID，仅限非Android Developer调试产品使用，Android Developer请clone整个项目进行编译】
> * 1、找到\app\src\main\res\raw目录下config.json文件
> * 2、将config.json文件中Hekr下AppId后的值改填成氦氪console平台的pid
> * 3、错误说明：不进行替换将出现Caused by: java.lang.NullPointerException: Hekr AppId is error in Config.json
> * 4、解释说明：登录氦氪console平台的账号应该具备定制app开发的权限，如无权限应该先申请

## DEMO 更新日志
|版本号|更新日志|
|:--|:--|
|2.2(13)|更新SDK,更新配网方法|
|2.1(12)|pid默认为空，运行demo前必须先填写在console上生成的pid|
|2.0(11)|更新SDK|
|1.9(10)|更新SDK|
|1.8(9)|在Application中启动个推服务，使用推送请替换自己的AppID、AppKey、AppSecret|
|1.7(8)|更新SDK,更新控制方式|
|1.6(7)|更新SDK,更新配网方法|
|1.5(6)|添加推送|
|1.4(5)|更新SDK|
|1.3(4)|增加邮箱注册|
|1.2(3)|修复收到返回信息后界面不能及时刷新的问题|
|1.1(2)|增加图形验证码|
|1.0(1)|初始版本|

## SDK 更新日志

### 1.2.2(28)
> * 局域网bug修复  

### 1.2.1(27)
> * 更新配网方法

### 1.1.5(24)
> * 修复sdk中post传入空entity导致的错误
> * 对外公开带Header[] http接口

### 1.1.4(23)
> * 修复sdk中上传图片接口偶然失败的bug

### 1.1.3(22)
> * 增加控制部分调用方法 sendMsg receiveDevSendMsg removeDevSendActionFilter clearAllActionFilter

### 1.1.2(21)
> * 添加web控制

### 1.0.4(18)
> * 配网方式修改
```
smartConfig.startConfig(ssid, pwd, 30);
smartConfig.setNewDeviceListener();
//修改为
smartConfig.startConfig(ssid, pwd, 30, new SmartConfig.NewDeviceListener());
```
> * hekrUserAction中增加一些新的接口
4.5.17 列举已上传文件
4.5.18 删除已上传文件
4.7 群组相关API


### 1.0.3(17)
> * 优化SDK体积

### 1.0.2(16)
> * SDK接口增加多语言

### 1.0.1(15)
> * hekrUserAction中增加一些新的接口


